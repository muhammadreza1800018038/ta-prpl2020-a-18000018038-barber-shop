<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Barbershop</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
    <?php 

include "connect.php";
$idtransaksi=$_GET['idtransaksi'];
$data = mysqli_query($connect,"SELECT * FROM transaksi where idtransaksi=$idtransaksi");
$data2 = mysqli_query($connect,"SELECT * FROM pelanggan join transaksi on pelanggan.username=transaksi.username join karyawan on karyawan.nik=pelanggan.nik where idtransaksi='$idtransaksi'");
$data3 = mysqli_query($connect,"SELECT * FROM paket join transaksi on paket.idpaket=transaksi.idpaket where idtransaksi='$idtransaksi'");

?>   
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
</header>
    <!--================Blog Area =================-->
        <div class="container">
            <div class="row">
            <div class="row col-md-6 col-md-offset-0 custyle">
               <p> Data customers </p>
    <table class="table table-striped custab">
    <thead>
        <tr>
            <th>ID Transaksi</th>
            <th>ID Paket</th>
            <th>Username</th>
            <th>Tanggal Booking</th>
            <th>Jam booking</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    <?php 
	foreach ($data as $x) {
		?>
            <tr>
            <td><?= $x['idtransaksi']; ?></td>
			<td><?= $x['idpaket']; ?></td>
			<td><?= $x['username']; ?></td>
			<td><?= $x['tanggalbooking']; ?></td>
      <td><?= $x['jambooking']; ?></td>
      <td><?= $x['status']; ?></td>
			<!--<td><?= $x['totalbayar']; ?></td>-->
            </tr>
            <?php
            };
            ?>
    </table>
    </tbody>
    </div>
            </div>
        </div>
        <div class="container">
            <div class="row col-md-6 col-md-offset-0 custyle">
    <table class="table table-striped custab">
    <thead>
        <tr>
            <th>Nama</th>
            <th>Hair artist </th>
            <th>nomor handphone</th>
        </tr>
    </thead>
    <tbody>
    <?php 
	foreach ($data2 as $x) {
		?>
            <tr>
			<td><?= $x['nama']; ?></td>
			<td><?= $x['nik']; ?></td>
			<td><?= $x['nomor_hp']; ?></td>
			<!--<td><?= $x['totalbayar']; ?></td>-->
            </tr>
            <?php
            };
            ?>
    </table>
    </tbody>
    </div>
        </div>
        <div class="container">
            <div class="row col-md-6 col-md-offset-0 custyle">
    <table class="table table-striped custab">
    <thead>
        <tr>
           
            <th>Paket</th>
            <th>Harga</th>
            <th>Durasi</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
    <?php 
	foreach ($data3 as $x) {
		?>
            <tr>
            <td><?= $x['namapaket']; ?></td>
			<td><?= $x['hargapaket']; ?></td>
			<td><?= $x['durasi']; ?></td>
			<td><?= $x['keterangan']; ?></td>
            </tr>
            <?php
            };
            ?>
    </table>
    </tbody>
    </div>
        </div>
        <div class="container">
            <div class="row col-md-6 col-md-offset-0 custyle">
            <h5>silahkan transfer ke rek berikut : 0710 529 957</h5>
                <h5>silahkan scrensot hasil transfer dan kirim data tersebut ke nomor berikut WA : 081253800872 (CS)</h5>
                <h5>Apabila telah dibayar dengan pas maka status akan berubah menjadi Sudah bayar</h5>
                <p>nb: data ini jangan lupa di bawa :)</p>
        </div>
        </div>
        
    <!--================Blog Area =================-->
<!-- link that opens popup -->
    <!-- JS here -->
    <script>
		window.print();
	</script>
</body>
</html>